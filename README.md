## Всем спасибо
это мой первый проект со структурой больше одного файла
### Необходимое для когов (модулей):
глобально везде нужен discord.py: `pip install discord`
#### speaker
для make_aware требуется django, а также настроенный django-сервер (путь к нему в config.py)
#### fun
для петттинга требуется библиотека petpetgif `pip install pet-pet-gif`
#### rando
для формульного рандома требуется модуль newrand (https://codeberg.org/assics/distandart), импортируйте его в ког как хотите
#### achivements
остро требуется django c модулями Person и Achvement (импортируйте их в utils/django_handler), а также Pillow `pip install pillow`
### Конфиги
config.py и messages.py ложатся в корень проекта - в первом лежат токен бота, путь до джанго и прочее (админ айпи по умолчанию моё); во втором хранятся веселые сообщения бота (спам в байки и ответы) с их настройками.