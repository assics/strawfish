import os
from config import bot_token, admin_id
import discord
from discord.ext import commands
import time

bot = commands.Bot(command_prefix='-',intents=discord.Intents.all())

@bot.event
async def on_ready():
    await bot.tree.sync()
    print('We have logged in as {0.user}'.format(bot))

@bot.event
async def setup_hook():
    for filename in os.listdir("./cogs"):
        if filename.endswith(".py"):
            await bot.load_extension(f"cogs.{filename[:-3]}")

@bot.tree.command(name="hh",description="hh")
async def hh(interaction: discord.Interaction):
    # 1/0
    await interaction.response.send_message("hh")

tree=bot.tree
@tree.error
async def on_command_error(ctx, error):
    print(error)
    ass=await ctx.guild.fetch_member(admin_id)
    try:
        await ctx.response.send_message(f":sob:{ass.mention}"+f"\n{str(error).split(':')[1].strip() if len(str(error).split(':'))>=2 else type(error)}")
    except:
        await ctx.send(f":sob:{ass.mention}"+f"\n{str(error).split(':')[1].strip() if len(str(error).split(':'))>=2 else type(error)}")

bot.run(bot_token)
