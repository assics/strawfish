
class Django_Handler:
    def __init__(self):
        from config import djroot
        import os, sys, django
        sys.path.append(djroot)
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djahw.settings")
        os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
        django.setup()
        from django.conf import settings
        self.settings=settings

    def timezone(self):
        self.settings.TIME_ZONE

    def models(self):
        from mare.models import Person, Achivement
        return Person, Achivement

