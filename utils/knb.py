import discord
class KNB(discord.ui.Modal):
    def __init__(self,user):
        self.user=user
        super().__init__(title='Чуз ё файтер')

    what = discord.ui.TextInput(label='Что', placeholder="камень/ножницы/бумага")

    async def on_submit(self, interaction: discord.Interaction):
        if "кам" in str(self.what).lower():
            what="кам"
        elif "нож" in str(self.what).lower():
            what="нож"
        elif "бум" in str(self.what).lower():
            what="бум"
        else:
            what=str(self.what)
        await interaction.response.send_message(f"Значит вы выбрали {what}.".replace("кам","камень").replace("бум","бумагу").replace("нож","ножницы"), ephemeral=True)
        h=False
        for du in duels:
            if du[0]==self.user:
                h=True
                tahw=du[2]
                duels.remove(du)
                await interaction.channel.send(f'{interaction.user.nick if interaction.user.nick else interaction.user.global_name} ответил на вызов {self.user.nick if self.user.nick else self.user.global_name}!\nЖдём сближения...')
                break
        if h:
            await asyncio.sleep(5)
            if what == tahw:
                await interaction.channel.send(f'Оба {self.user.mention} и {interaction.user.mention} пришли на дуэль с {tahw}.\nСлишком много общего для борьбы. Полагаю, это ничья.'.replace("кам","камнем").replace("бум","бумагой").replace("нож","ножницами"))
            elif "кам" not in [what,tahw] and "нож" not in [what,tahw] and "бум" not in [what,tahw]:
                await interaction.channel.send(f'{self.user.mention} и {interaction.user.mention} пришли на дуэль с {tahw} и {what}.\nСудья ничего не понял и ушёл.')
            else:
                await interaction.channel.send(f'В руках {self.user.nick if self.user.nick else self.user.global_name} - {tahw}. У {interaction.user.nick if interaction.user.nick else interaction.user.global_name} - {what}.\nОставим победу за { interaction.user.mention if (what=="кам" and tahw=="нож") or (what=="нож" and tahw=="бум") or (what=="бум" and tahw=="кам") or (tahw not in ["кам","бум","нож"]) else self.user.mention }'.replace("кам","камень").replace("бум","бумага").replace("нож","ножницы"))
        else:
            duels.append([interaction.user,self.user,what])
            await interaction.channel.send(f'{self.user.mention}!\nВам бросили вызов!\nОтвечайте /duel {interaction.user.mention}')
