import discord
from discord import app_commands
from discord.ext import commands

import sys
sys.path.append("..")
from distandart.newrand import rand #any other way to newrand package

class Rando(commands.Cog):

    def __init__(self,bot):
        self.bot=bot

    @app_commands.command(name="randfor",description="убойный рандом формулами")
    @app_commands.describe(form="математическая формула python, но с d123, d12:23, 50c100, { }123")
    async def randfor(self,interaction: discord.Interaction, form: str):
        try:
            res=rand(form)
            embed = discord.Embed(title="Suм: "+str(res[0]),description="```\n"+" ".join([str(x) for x in res[1]])+"\n```",color=0x26a269)
            embed.set_footer(text=form)
        except Exception as e:
            print(e)
            embed = discord.Embed(description=random.choice(":(","нипонимаю...","*результат*","reference=None","кар отстань"))
        try:
            await interaction.response.send_message(embed=embed)
        except:
            try:
                await interaction.channel.send(embed=embed)
            except:
                await interaction.channel.send(f"короче, сумма: {res[0]}")

async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Rando(bot))
