import discord
from discord import app_commands
from discord.ext import commands

from petpetgif import petpet
from io import BytesIO
from utils.knb import KNB
from zalgolib.zalgolib import enzalgofy

class Entertainer(commands.Cog):

    def __init__(self,bot):
        self.bot=bot
        self.ctx_menu = app_commands.ContextMenu(
            name='headpet',
            callback=self.peteptCM,
        )
        self.bot.tree.add_command(self.ctx_menu)
        self.duels=[]

    @app_commands.command(name="zalgo",description="oh hi rudd!")
    @app_commands.describe(text="текстовый текст")
    @app_commands.describe(core="степень корёжа (1-100)")
    async def zauza(self,interaction: discord.Interaction, text: str, core: int = 50):
        core%=101
        await interaction.response.send_message(enzalgofy(text,core))


    async def peteptCM(self,interaction: discord.Interaction, user: discord.User) -> None:
        image = await user.display_avatar.read()
        source = BytesIO(image) # file-like container to hold the emoji in memory
        dest = BytesIO() # container to store the petpet gif in memory
        petpet.make(source, dest)
        dest.seek(0) # set the file pointer back to the beginning so it doesn't upload a blank file.
        await interaction.response.send_message(file=discord.File(dest, filename=f"{image[0]}-petpet.gif"))

    @app_commands.command(name="headpet",description="они это заслужили")
    @app_commands.describe(user="каво?")
    @app_commands.describe(pic="картинка? (нейтрализует имя)")
    async def petept(self,interaction: discord.Interaction, user: discord.User, pic: discord.Attachment = None):
        if pic:
            fn=pic.filename
            await pic.save(fn)
            f=Image.open(fn)
            f=f.resize((min(f.size),min(f.size)))
            f.save(fn)
            f.close()
            image = open(fn,"rb").read()
        else:
            image = await user.display_avatar.read()
        source = BytesIO(image) # file-like container to hold the emoji in memory
        dest = BytesIO() # container to store the petpet gif in memory
        petpet.make(source, dest)
        dest.seek(0) # set the file pointer back to the beginning so it doesn't upload a blank file.
        await interaction.response.send_message(file=discord.File(dest, filename=f"{image[0]}-petpet.gif"))

    @app_commands.command(name="duel",description="бросить вызов/ответить наглицу")
    @app_commands.describe(user="каму?")
    async def duel(self,interaction: discord.Interaction, user: discord.User):
        await interaction.response.send_modal(KNB(user))

async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Entertainer(bot))
