import discord
from discord import app_commands
from discord.ext import commands

from utils.django_handler import Django_Handler
from config import achivements_site

from django.core.files import File
from django.utils.timezone import make_aware
import time
from datetime import datetime,timedelta
import os
from datetime import timedelta
from io import BytesIO
from PIL import Image
import random

async def achshow(interaction,a,user,dtex=""):
    if type(a) != list:
        a=[a]
    embeds=[]
    files=[]
    for an in a:
        embed = discord.Embed(title=an.title, description=an.text, color=user.color, timestamp=an.date+timedelta(hours=random.randint(0,23),minutes=random.randint(0,59)),url=f"{achivements_site}/{user.id}/#{an.title.replace(' ','')}")
        files.append(discord.File(an.pic.path, filename=an.pic.name.split("/")[-1]))
        embed.set_thumbnail(url="attachment://"+an.pic.name.split("/")[-1])#Делай через сайт пж
        embeds.append(embed)
    await interaction.response.send_message(dtex,embeds=embeds,files=files)

class Archer(commands.Cog):

    def __init__(self,bot):
        self.bot=bot
        handler=Django_Handler()
        self.Person=handler.models()[0]
        handler.timezone()
        print(self.Person.objects.all())

        self.ctx_menu1 = app_commands.ContextMenu(
            name='all achivements',
            callback=self.userachsCM,
        )
        self.bot.tree.add_command(self.ctx_menu1)

        self.ctx_menu2 = app_commands.ContextMenu(
            name='show all achivements',
            callback=self.shuserachsCM,
        )
        self.bot.tree.add_command(self.ctx_menu2)

    async def userachsCM(self,interaction: discord.Interaction, user: discord.User) -> None:
        try:
            p=self.Person.objects.get(did=user.id)
            await interaction.response.send_message(f":saluting_face: {p.name}\n- "+"\n- ".join([f"[{x.title}]({achivements_site}/{user.id}/#{x.title.replace(' ','')})" for x in p.achivement_set.all()]))
        except:
            await interaction.response.send_message(random.choice(["не вем","не знам","не"]))

    async def shuserachsCM(self,interaction: discord.Interaction, user: discord.User) -> None:
        try:
            p=self.Person.objects.get(did=user.id)
        except:
            await interaction.response.send_message(random.choice(["не вем","не знам","не"]))
            return 0
        await achshow(interaction,list(p.achivement_set.all()),user,"\n:clap:"+p.name+":clap:")

    group = app_commands.Group(name="achivements", description="achiving commands")

    @group.command(name="all",description="все медальки")
    @app_commands.describe(user="каво?")
    async def userachscd(self,interaction: discord.Interaction, user: discord.User):
        try:
            p=self.Person.objects.get(did=user.id)
            await interaction.response.send_message(f":saluting_face: {p.name}\n- "+"\n- ".join([f"[{x.title}]({achivements_site}/{user.id}/#{x.title.replace(' ','')})" for x in p.achivement_set.all()]))
        except:
            await interaction.response.send_message(random.choice(["не вем","не знам","не"]))

    @group.command(name="show-all",description="рассмотреть все медальки")
    @app_commands.describe(user="каво?")
    async def shuserachscd(self,interaction: discord.Interaction, user: discord.User):
        try:
            p=self.Person.objects.get(did=user.id)
        except:
            await interaction.response.send_message(random.choice(["не вем","не знам","не"]))
            return 0
        await achshow(interaction,list(p.achivement_set.all()),user,"\n:clap:"+p.name+":clap:")

    @group.command(name="show-one",description="рассмотреть медальку")
    @app_commands.describe(user="каво?")
    @app_commands.describe(title="чево? (название)")
    async def userach(self,interaction: discord.Interaction, user: discord.User, title: str):
        try:
            p=self.Person.objects.get(did=user.id)
            a=p.achivement_set.get(title=title)
            # await interaction.response.send_message(a.title+"\n"+a.text+"\n"+str(a.date),file=discord.File(a.pic.path))
        except:
            await interaction.response.send_message(random.choice(["не вем тако","не знам тако","не"]))
            return 0
        await achshow(interaction,a,user,"\n:clap:"+p.name+":clap:")

    @app_commands.command(description="удостоить")
    @app_commands.describe(user="каво?")
    @app_commands.describe(title="как?")
    @app_commands.describe(text="что?")
    @app_commands.describe(pic="картинка (чем квадратнее тем лучше)")
    @app_commands.describe(date="когды то было? (дата ГГГГ-ММ-ДД)")
    async def achivement(self,interaction: discord.Interaction, user: discord.User, title: str, text: str, pic: discord.Attachment, date: str = time.strftime('%Y-%m-%d',time.localtime())):
        date=make_aware(datetime.strptime(date,'%Y-%m-%d'))
        try:
            p=self.Person.objects.get(did=user.id)
        except:
            fn=user.global_name+"-ava.png" if user.global_name else user.name+"-ava.png"
            await user.display_avatar.save(fn)
            f = open(fn,"rb")
            p=self.Person(name=user.nick if user.nick else user.global_name if user.global_name else user.name,did=user.id,pic=File(f,name=fn))
            p.save()
            f.close()
            os.remove(fn)
        if not title in [x.title for x in p.achivement_set.all()]:
            fn=pic.filename
            if fn.split(".")[-1] not in ["jpeg","jpg","png","gif","webm"]:
                fn=fn+".png"
            await pic.save(fn)
            f=Image.open(fn)
            f=f.resize((min(f.size),min(f.size)))
            f.save(fn)
            f = open(fn,"rb") #,encoding="utf-8", errors="ignore"
            a=p.achivement_set.create(title=title, text=text, pic=File(f,name=fn), date=date)
            f.close()
            os.remove(fn)
            await achshow(interaction,a,user,"\n:clap:"+user.mention+":clap:")
            # await interaction.response.send_message(a.title+"\n"+a.text+"\n"+str(a.date),file=discord.File(a.pic.path))
        else:
            a=p.achivement_set.get(title=title)
            await achshow(interaction,a,user,"а до нас тако уже есть дада")
            # await interaction.response.send_message(a.title+"\n"+a.text+"\n"+str(a.date),file=discord.File(a.pic.path))

    @group.command(name="rename-me",description="смена имени в списках")
    @app_commands.describe(user="кто я?")
    @app_commands.describe(name="как я? (а иначе то что видим)")
    async def rename(self,interaction: discord.Interaction, user: discord.User, name: str | None):
        if user.id==interaction.user.id or interaction.user.guild_permissions.administrator:
            if name==None: name=user.nick if user.nick else user.global_name
            try:
                p=self.Person.objects.get(did=user.id)
                p.name=name
                p.save()
            except:
                fn=user.global_name+"-ava.png"
                await user.display_avatar.save(fn)
                f = open(fn,"rb")
                p=self.Person(name=name,did=user.id,pic=File(f,name=fn))
                p.save()
                f.close()
                os.remove(fn)
            await interaction.response.send_message(f"[ок!]({achivements_site}/{p.did}/)")
        else:
            await interaction.response.send_message("я тебе не верю")

    @group.command(name="reshow-me",description="смена аватарки в списках")
    @app_commands.describe(user="кто я?")
    async def repic(self,interaction: discord.Interaction, user: discord.User):
        if user.id==interaction.user.id or interaction.user.guild_permissions.administrator:
            try:
                p=self.Person.objects.get(did=user.id)
                fn=user.global_name+"-ava.png"
                await user.display_avatar.save(fn)
                f = open(fn,"rb")
                p.pic=File(f,name=fn)
                p.save()
                f.close()
                os.remove(fn)
            except:
                fn=user.global_name+"-ava.png"
                await user.display_avatar.save(fn)
                f = open(fn,"rb")
                p=self.Person(name=user.nick if user.nick else user.global_name,did=user.id,pic=File(f,name=fn))
                p.save()
                f.close()
                os.remove(fn)
            await interaction.response.send_message(f"[ок!]({achivements_site}/{p.did}/)")
        else:
            await interaction.response.send_message("я тебе не верю")


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Archer(bot))
