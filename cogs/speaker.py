import discord
from discord.ext import commands,tasks

from django.utils.timezone import make_aware
from utils.django_handler import Django_Handler
import messages as ms
from datetime import datetime,timedelta
import random
from importlib import reload


class Speaker(commands.Cog):

    def __init__(self,bot):
        handler=Django_Handler()
        handler.timezone()
        self.bot=bot
        self.prevmesstime=make_aware(datetime.now())
        self.anmess=[]
        self.baer.start()

    @tasks.loop(hours=ms.bajka.delay)
    async def baer(self):
        reload(ms)
        await self.bot.wait_until_ready()
        bae=self.bot.get_channel(ms.bajka.channel_id)
        pr=[message async for message in bae.history(limit=1)][0]
        print(make_aware(datetime.now())-pr.created_at)
        if make_aware(datetime.now())-pr.created_at > ms.bajka.padding and not pr.author.bot and len(pr.content)<1000 and len(pr.attachments)==0:
            mess = random.choice(ms.bajka.messages)
            await bae.send(mess)

    @commands.Cog.listener()
    async def on_message(self,message):
        if self.bot.user.mentioned_in(message):
            reload(ms)
            if message.author.id in ms.reply.special_messages and random.randint(0,ms.reply.chance_of_special[1])<=ms.reply.chance_of_special[0]:
                mess=ms.reply.special_messages[message.author.id]
            else:
                mess=ms.reply.messages
            mess*=int(len(ms.reply.messages)/len(mess)) if int(len(ms.reply.messages)/len(mess))!=0 else 1

            pnh=False
            if message.created_at - self.prevmesstime < ms.reply.annoy.delay:
                self.anmess+=[random.choice(ms.reply.annoy.messages) for x in range(ms.reply.annoy.mess_per_annoy)]
                mess+=self.anmess
                if random.randint(0,4)==2:
                    pnh=True
            else:
                self.anmess=[]
            if not pnh:
                await message.channel.send(random.choice(mess),reference=message)
            self.prevmesstime=message.created_at

        if not message.content.startswith("http"):
            for part in ms.resp.parts:
                if part in message.content.lower():
                    await message.channel.send(random.choice(ms.resp.parts[part]))
        if message.content in ms.resp.fulls:
            await message.channel.send(random.choice(ms.resp.fulls[message.content]))

    @commands.command(name='spp')
    async def spp(self, ctx, id, what):
        where = self.bot.get_channel(int(id))
        await where.send(what)

async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Speaker(bot))
